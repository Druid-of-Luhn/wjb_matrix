SRC_DIR ?= src
INC_DIR ?= include
INCLUDE ?= wjb_matrix.h
OBJ_DIR ?= obj
BUILD_DIR ?= lib
TARGET ?= $(BUILD_DIR)/libwjbm.a
TEST_EXEC ?= wjb_test

# Ignore test files when compiling sources
SRCS := $(shell find $(SRC_DIR) ! -name \*_test.c ! -name tests.c -name \*.c)
OBJS := $(SRCS:$(SRC_DIR)/%.c=$(OBJ_DIR)/%.o)
# Compiling tests checks source files as well
TESTS := $(shell find $(SRC_DIR) -name \*_test.c -or -name tests.c)

CFLAGS += -std=c11 -Wall -Wextra

# Build the static library
all: $(SRC_DIR)/$(INCLUDE) $(OBJS)
	@# Copy the include file to the include directory
	@mkdir -p $(INC_DIR)
	cp $(SRC_DIR)/$(INCLUDE) $(INC_DIR)/$(INCLUDE)
	@# Bundle the object files into a static library
	@mkdir -p $(BUILD_DIR)
	ar rcs $(TARGET) $(OBJS)

# Compile and run the tests
test: $(TESTS)
	$(CC) $^ -o $(TEST_EXEC) $(CFLAGS)
	@./$(TEST_EXEC)
	@rm -rf $(TEST_EXEC)*

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	@mkdir -p $(OBJ_DIR)
	$(CC) -c $< -o $@ $(CFLAGS)

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR) $(OBJ_DIR) $(INC_DIR) $(TEST_EXEC)*
