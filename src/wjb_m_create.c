/*
 * Copyright Billy Brown 2016
 *
 * This file is part of libwjbm.
 *
 * libwjbm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwjbm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwjbm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include "wjb_matrix.h"

wjb_matrix
wjb_m_make(const unsigned int rows, const unsigned int cols)
{
    // Allocate the space for the data
    double *data = calloc(rows * cols, sizeof(double));
    // Return an empty matrix if out of memory
    if (data == NULL) {
        const wjb_matrix error = {
            .rows = 0,
            .cols = 0,
            .data = NULL
        };
        return error;
    }
    // Otherwise build the matrix with the data
    const wjb_matrix matrix = {
        .rows = rows,
        .cols = cols,
        .data = data
    };
    return matrix;
}

wjb_matrix
wjb_m_copy(const wjb_matrix source)
{
    // Make sure the source is a valid matrix
    if (source.rows == 0 || source.cols == 0 || source.data == NULL) {
        // The source is an error matrix, so return that
        return source;
    }
    // Create the new matrix, with the same dimensions
    wjb_matrix dest = wjb_m_make(source.rows, source.cols);
    // Make sure it is valid
    if (dest.rows == 0 || dest.cols == 0 || dest.data == NULL) {
        return dest;
    }
    // Copy the data from the source over to the destination
    memcpy(dest.data, source.data, dest.rows * dest.cols);
    return dest;
}

void
wjb_m_free(wjb_matrix *matrix)
{
    // If the matrix has data, free it
    if (matrix->data != NULL) {
        free(matrix->data);
        // Make the matrix invalid
        matrix->data = NULL;
    }
}

int
wjb_m_valid(const wjb_matrix matrix)
{
    // Make sure all of the values in the matrix structure are valid
    if (matrix.rows > 0 && matrix.cols > 0 && matrix.data != NULL) {
        return 1;
    }
    return 0;
}
