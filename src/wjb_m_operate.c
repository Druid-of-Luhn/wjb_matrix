/*
 * Copyright Billy Brown 2016
 *
 * This file is part of libwjbm.
 *
 * libwjbm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwjbm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwjbm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "wjb_matrix.h"

wjb_matrix
wjb_m_add(const wjb_matrix matrix, const double value)
{
    // Make sure the matrix is valid first
    if (!wjb_m_valid(matrix)) {
        return matrix;
    }
    // Create the resulting matrix, with the same dimensions
    wjb_matrix result = wjb_m_make(matrix.rows, matrix.cols);
    // Add to all of the values
    for (unsigned int i = 1; i <= result.rows; ++i) {
        for (unsigned int j = 1; j <= result.cols; ++j) {
            const double current = wjb_m_get(matrix, i, j);
            wjb_m_set(result, i, j, current + value);
        }
    }
    return result;
}

wjb_matrix
wjb_m_sum(const wjb_matrix m1, const wjb_matrix m2)
{
    // Make sure the matrices are valid and the dimensions are right
    if (!wjb_m_valid(m1) || !wjb_m_valid(m2) ||
        m1.rows != m2.rows || m1.cols != m2.cols) {
        return wjb_m_make(0, 0);
    }
    // Create the new resulting matrix
    wjb_matrix m = wjb_m_make(m1.rows, m1.cols);
    // Visit every one of it's cells
    for (unsigned int i = 1; i <= m.rows; ++i) {
        for (unsigned int j = 1; j <= m.cols; ++j) {
            // Sum the values
            const double val1 = wjb_m_get(m1, i, j);
            const double val2 = wjb_m_get(m2, i, j);
            wjb_m_set(m, i, j, val1 + val2);
        }
    }
    return m;
}

wjb_matrix
wjb_m_multiply(const wjb_matrix matrix, const double value)
{
    // Make sure the matrix is valid first
    if (!wjb_m_valid(matrix)) {
        return matrix;
    }
    // Create the resulting matrix, with the same dimensions
    wjb_matrix result = wjb_m_make(matrix.rows, matrix.cols);
    // Add to all of the values
    for (unsigned int i = 1; i <= result.rows; ++i) {
        for (unsigned int j = 1; j <= result.cols; ++j) {
            const double current = wjb_m_get(matrix, i, j);
            wjb_m_set(result, i, j, current * value);
        }
    }
    return result;
}

wjb_matrix
wjb_m_product(const wjb_matrix m1, const wjb_matrix m2)
{
    // Make sure the matrices are valid and the dimensions are right
    if (!wjb_m_valid(m1) || !wjb_m_valid(m2) || m1.cols != m2.rows) {
        return wjb_m_make(0, 0);
    }
    // Create the new resulting matrix
    wjb_matrix m = wjb_m_make(m1.rows, m2.cols);
    unsigned int index = 0;
    // Visit every one of it's cells
    for (unsigned int i = 1; i <= m.rows; ++i) {
        for (unsigned int j = 1; j <= m.cols; ++j) {
            m.data[index] = 0;
            // Iterate over m1's cols and m2's rows
            for (unsigned int k = 1; k <= m1.cols; ++k) {
                // A_i,j = sum_k(B_i,k * C_k,j)
                m.data[index] += wjb_m_get(m1, i, k) * wjb_m_get(m2, k, j);
            }
            ++index;
        }
    }
    return m;
}
