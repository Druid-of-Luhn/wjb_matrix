/*
 * Copyright Billy Brown 2016
 *
 * This file is part of libwjbm.
 *
 * libwjbm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwjbm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwjbm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WJB_MATRIX_H
#define WJB_MATRIX_H

#include <stdio.h>

/**
 * Structure representing a 'rows'x'cols' matrix, with a
 * single-dimension array holding its data.
 */
typedef struct WJB_MATRIX {
    const unsigned int rows;
    const unsigned int cols;
    double *data;
} wjb_matrix;

/**
 * Function that computes a double from a double.
 */
typedef double (*wjb_m_function)(const double);

/**
 * Create a matrix structure and allocate its data array. Requires
 * de-allocation with 'wjb_m_free'.
 * @param rows the number of rows in the matrix
 * @param cols the number of columns in the matrix
 * @return wjb_matrix a matrix of the correct dimensions and with
 * allocated memory, or a matrix with rows and cols = 0 on error
 */
wjb_matrix
wjb_m_make(const unsigned int rows, const unsigned int cols);

/**
 * Copy a matrix into a new separate structure. Allocates memory
 * for the new matrix, which requires de-allocation.
 * @param wjb_matrix the source matrix to copy
 * @return wjb_matrix the copied matrix
 */
wjb_matrix
wjb_m_copy(const wjb_matrix source);

/**
 * Free a matrix's allocated memory.
 * @param matrix the matrix to free
 */
void
wjb_m_free(wjb_matrix *matrix);

/**
 * Determine whether a matrix is valid or not.
 * @param matrix the matrix to test
 * @return int 1 if it is valid, 0 otherwise
 */
int
wjb_m_valid(const wjb_matrix matrix);

/**
 * Get the value at row i and column j.
 * @param matrix the matrix to fetch from
 * @param i the row number to fetch from (starting at 1)
 * @param j the column number to fetch from (starting at 1)
 * @return double the value at the ith row and jth column, or 0
 */
double
wjb_m_get(const wjb_matrix matrix, const unsigned int i, const unsigned int j);

/**
 * Get a pointer to row i and column j.
 * @param matrix the matrix to fetch from
 * @param i the row number to fetch from (starting at 1)
 * @param j the column number to fetch from (starting at 1)
 * @return double* a pointer to the ith row and jth column, or NULL
 */
double *
wjb_m_getp(const wjb_matrix matrix, const unsigned int i, const unsigned int j);

/**
 * Set the value at row i and column j, or do nothing if out of range.
 * @param matrix the matrix in which to set the value
 * @param i the row number to set at (starting at 1)
 * @param j the column number to set at (starting at 1)
 * @param value the value to set
 */
void
wjb_m_set(wjb_matrix matrix, const unsigned int i, const unsigned int j,
        const double value);

/**
 * Map a function over all values in the matrix.
 * @param matrix the matrix to be mapped over
 * @param func the function to map over the matrix
 * @return wjb_matrix a new matrix with the values returned from func
 */
wjb_matrix
wjb_m_map(const wjb_matrix matrix, wjb_m_function func);

/**
 * Add a constant to all values in a matrix.
 * @param matrix the matrix containing the values to be added to
 * @param value the value to add to those in the matrix
 * @return wjb_matrix a new matrix containing the calculated values
 */
wjb_matrix
wjb_m_add(const wjb_matrix matrix, const double value);

/**
 * Calculate the sum of two matrices.
 * @param m1 the matrix to the left side of the sum (m x n)
 * @param m2 the matrix to the right side of the sum (m x n)
 * @return wjb_matrix a new matrix which is the sum of the two
 * parameters (m x n)
 */
wjb_matrix
wjb_m_sum(const wjb_matrix m1, const wjb_matrix m2);

/**
 * Multiply all values in a matrix by a constant.
 * @param matrix the matrix containing the values to be multiplied to
 * @param value the value to multiply those in the matrix by
 * @return wjb_matrix a new matrix containing the calculated values
 */
wjb_matrix
wjb_m_multiply(const wjb_matrix matrix, const double value);

/**
 * Calculate the product of two matrices.
 * @param m1 the matrix to the left side of the multiplication (m x n)
 * @param m2 the matrix to the right side of the multiplication (n x p)
 * @return wjb_matrix a new matrix which is the product of the two
 * parameters (m x p)
 */
wjb_matrix
wjb_m_product(const wjb_matrix m1, const wjb_matrix m2);

/**
 * Print a matrix to the standard output.
 * @param matrix the matrix to display
 * @param format the format string to use on every value
 */
void
wjb_m_print(const wjb_matrix matrix, const char *format);

/**
 * Write a matrix to a file.
 * @param file the file to write to.
 * @param matrix the matrix to display
 * @param format the format string to use on every value
 */
void
wjb_m_fprint(FILE *file, const wjb_matrix matrix, const char *format);

#endif
