/*
 * Copyright Billy Brown 2016
 */

#include "minunit.h"
#include "wjb_m_create.c"

static char *
m_make_test(void)
{
    wjb_matrix m1 = wjb_m_make(0, 0);
    mu_assert(
            "[wjb_matrix] ERROR m_make_test @ wjb_m_make(0, 0)\n"
            "             expected rows == 0 && cols == 0",
            m1.rows == 0 && m1.rows == 0);
    wjb_m_free(&m1);

    wjb_matrix m2 = wjb_m_make(4, 3);
    mu_assert(
            "[wjb_matrix] ERROR m_make_test @ wjb_m_make(4, 3)\n"
            "             expected rows == 4 && cols == 3",
            m2.rows == 4 && m2.cols == 3);
    wjb_m_free(&m2);

    return 0;
}

static char *
m_copy_test(void)
{
    wjb_matrix m1 = wjb_m_make(4, 3);
    // Set a value in m1
    m1.data[0] = 5;
    wjb_matrix m2 = wjb_m_copy(m1);
    mu_assert(
            "[wjb_matrix ERROR m_copy_test @ wjb_m_valid(m2)\n"
            "            expected 1 (true)",
            wjb_m_valid(m2));
    mu_assert(
            "[wjb_matrix] ERROR m_copy_test @ equal rows\n"
            "             expected m1.rows == m2.rows",
            m1.rows == m2.rows);
    mu_assert(
            "[wjb_matrix] ERROR m_copy_test @ equal cols\n"
            "             expected m1.cols == m2.cols",
            m1.cols == m2.cols);
    mu_assert(
            "[wjb_matrix] ERROR m_copy_test @ different pointer\n"
            "             expected m1.data != m2.data",
            m1.data != m2.data);
    mu_assert(
            "[wjb_matrix] ERROR m_copy_test @ same data\n"
            "             expected m1.data[0] == m2.data[0]",
            m1.data[0] == m2.data[0]);
    // Change the value in m1
    m1.data[0] = 3;
    mu_assert(
            "[wjb_matrix ERROR m_copy_test @ different data\n"
            "            expected m1.data[0] != m2.data[0]",
            m1.data[0] != m2.data[0]);

    return 0;
}

static char *
m_valid_test(void)
{
    wjb_matrix m1 = wjb_m_make(0, 0);
    mu_assert(
            "[wjb_matrix] ERROR m_valid_test @ wjb_m_valid(m1)\n"
            "             expected 0 (false)",
            !wjb_m_valid(m1));
    wjb_m_free(&m1);
    
    wjb_matrix m2 = wjb_m_make(4, 3);
    mu_assert(
            "[wjb_matrix] ERROR m_valid_test @ wjb_m_valid(m2)\n"
            "             expected 1 (true)",
            wjb_m_valid(m2));
    wjb_m_free(&m2);

    return 0;
}

char *
m_create_tests(void)
{
    mu_run_test(m_make_test);
    mu_run_test(m_copy_test);
    mu_run_test(m_valid_test);

    return 0;
}
