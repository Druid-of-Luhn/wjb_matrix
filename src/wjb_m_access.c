/*
 * Copyright Billy Brown 2016
 *
 * This file is part of libwjbm.
 *
 * libwjbm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwjbm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwjbm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stddef.h>

#include "wjb_matrix.h"

// Convert row and column numbers to a linear index value
static int
m_index(const wjb_matrix matrix, const unsigned int i, const unsigned int j)
{
    // Make sure the index is in bounds
    if (i > 0 && i <= matrix.rows && j > 0 && j <= matrix.cols) {
        return (i - 1) * matrix.cols + (j - 1);
    }
    return -1;
}

double
wjb_m_get(const wjb_matrix matrix, const unsigned int i, const unsigned int j)
{
    const int index = m_index(matrix, i, j);
    if (index >= 0 && wjb_m_valid(matrix)) {
        return matrix.data[index];
    }
    return 0;
}

double *
wjb_m_getp(const wjb_matrix matrix, const unsigned int i, const unsigned int j)
{
    const int index = m_index(matrix, i, j);
    if (index >= 0 && wjb_m_valid(matrix)) {
        return matrix.data + index;
    }
    return NULL;
}

void
wjb_m_set(wjb_matrix matrix, const unsigned int i, const unsigned int j,
        const double value)
{
    const int index = m_index(matrix, i, j);
    if (index >= 0 && wjb_m_valid(matrix)) {
        matrix.data[index] = value;
    }
}

wjb_matrix
wjb_m_map(const wjb_matrix matrix, wjb_m_function func)
{
    // The new matrix has the same dimensions as the original
    wjb_matrix result = wjb_m_make(matrix.rows, matrix.cols);
    if (!wjb_m_valid(result)) {
        return result;
    }
    // Iterate over all elements in the matrix
    for (unsigned int i = 0; i < matrix.rows * matrix.cols; ++i) {
        result.data[i] = func(matrix.data[i]);
    }
    return result;
}
