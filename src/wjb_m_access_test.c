/*
 * Copyright Billy Brown 2016
 */

#include "minunit.h"
#include "wjb_m_access.c"

static char *
m_get_test(void)
{
    wjb_matrix m1 = wjb_m_make(4, 3);
    m1.data[1] = 3;
    mu_assert(
            "[wjb_matrix] ERROR m_get_test @ wjb_m_get(m1, 1, 1)\n"
            "             expected 3",
            wjb_m_get(m1, 1, 2) == 3);
    // Off-by-one error
    m1.data[0] = 3;
    mu_assert(
            "[wjb_matrix] ERROR m_get_test @ wjb_m_get(m1, 0, 0)\n"
            "             expected 0",
            wjb_m_get(m1, 0, 0) == 0);
    wjb_m_free(&m1);

    wjb_matrix m2 = wjb_m_make(0, 0);
    mu_assert(
            "[wjb_matrix] ERROR m_getp_test @ wjb_m_get(m2, 1, 1)\n"
            "             expected 0",
            wjb_m_get(m2, 1, 1) == 0);
    wjb_m_free(&m2);

    return 0;
}

static char *
m_getp_test(void)
{
    wjb_matrix m = wjb_m_make(4, 3);
    m.data[2] = 3;
    mu_assert(
            "[wjb_matrix] ERROR m_getp_test @ wjb_m_getp(m, 1, 3)\n"
            "             expected pointers equal",
            wjb_m_getp(m, 1, 3) == &(m.data[2]));
    mu_assert(
            "[wjb_matrix] ERROR m_getp_test @ wjb_m_getp(m, 1, 3)\n"
            "             expected 3",
            *(wjb_m_getp(m, 1, 3)) == 3);
    wjb_m_free(&m);

    // Get after free
    mu_assert(
            "[wjb_matrix] ERROR m_get_test @ get after free\n"
            "             expected 0",
            wjb_m_get(m, 1, 3) == 0);
    mu_assert(
            "[wjb_matrix] ERROR m_get_test @ getp after free\n"
            "             expected NULL",
            wjb_m_getp(m, 1, 3) == NULL);

    return 0;
}

static char *
m_set_test(void)
{
    wjb_matrix m = wjb_m_make(4, 3);

    wjb_m_set(m, 1, 2, 5);
    mu_assert(
            "[wjb_matrix] ERROR m_set_test @ wjb_m_set(m, 1, 2, 5)\n"
            "             expected 5",
            wjb_m_get(m, 1, 2) == 5);
    // Off-by-one error
    m.data[0] = 5;
    wjb_m_set(m, 0, 0, 3);
    mu_assert(
            "[wjb_matrix] ERROR m_set_test @ wjb_m_set(m, 0, 0, 3)\n"
            "             expected 5",
            m.data[0] == 5);

    wjb_m_set(m, m.rows, m.cols, 3);
    mu_assert(
            "[wjb_matrix] ERROR m_set_test @ wjb_m_set(m, m.rows, m.cols, 3)\n"
            "             expected 3",
            wjb_m_get(m, m.rows, m.cols) == 3);

    wjb_m_free(&m);

    // Set after free
    wjb_m_set(m, 1, 1, 3);
    mu_assert(
            "[wjb_matrix] ERROR m_set_test @ set after free\n"
            "             expected no error",
            1);

    return 0;
}

static double
double_func(const double value)
{
    return value * 2;
}

static char *
m_map_test(void)
{
    wjb_matrix m1 = wjb_m_make(2, 2);

    wjb_m_set(m1, 1, 1, 1); wjb_m_set(m1, 1, 2, 2);
    wjb_m_set(m1, 2, 1, 3); wjb_m_set(m1, 2, 2, 4);

    wjb_matrix m2 = wjb_m_map(m1, double_func);

    // Make sure the function was applied to all values
    mu_assert(
            "[wjb_matrix] ERROR m_map_test @ wjb_m_map(m1, double_func)\n"
            "expected 2, 4, 6, 8",
            wjb_m_get(m2, 1, 1) == 2 && wjb_m_get(m2, 1, 2) == 4 &&
            wjb_m_get(m2, 2, 1) == 6 && wjb_m_get(m2, 2, 2) == 8);
    // Make sure the original matrix was not mutated
    mu_assert(
            "[wjb_matrix ERROR m_map_test @ wjb_m_get(m1, 2, 1)\n"
            "expected 3",
            wjb_m_get(m1, 2, 1) == 3);

    wjb_m_free(&m1);
    wjb_m_free(&m2);

    return 0;
}

char *
m_access_tests(void)
{
    mu_run_test(m_get_test);
    mu_run_test(m_getp_test);
    mu_run_test(m_set_test);
    mu_run_test(m_map_test);
    return 0;
}
