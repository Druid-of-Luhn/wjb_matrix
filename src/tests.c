/*
 * Copyright Billy Brown 2016
 */

#include <stddef.h>
#include <stdio.h>

#include "minunit.h"

extern char * m_create_tests(void);
extern char * m_access_tests(void);
extern char * m_operate_tests(void);

int tests_run = 0;

static char *
tests(void)
{
    char * error = NULL;

    if ((error = m_create_tests())) {
        return error;
    }
    if ((error = m_access_tests())) {
        return error;
    }
    if ((error = m_operate_tests())) {
        return error;
    }

    return NULL;
}

int
main(void)
{
    // Run the tests
    char *matrix = tests();
    // Print the error if one occurred
    if (matrix != NULL) {
        printf("%s\n", matrix);
        return 1;
    } else {
        // Otherwise notify success
        printf("[wjb_matrix] all tests passed\n");
    }

    printf(" [wjb_tests] ran %d test%s\n",
            tests_run, tests_run == 1 ? "" : "s");

    return 0;
}
