/*
 * Copyright Billy Brown 2016
 *
 * This file is part of libwjbm.
 *
 * libwjbm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libwjbm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libwjbm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

#include "wjb_matrix.h"

static char
left_bracket(const wjb_matrix matrix, unsigned int i)
{
    if (!wjb_m_valid(matrix)) {
        return 0;
    }
    if (matrix.rows == 1) {
        return '<';
    }
    if (i == 1) {
        return '/';
    }
    if (i == matrix.rows) {
        return '\\';
    }
    return '|';
}

static char
right_bracket(const wjb_matrix matrix, unsigned int i)
{
    if (!wjb_m_valid(matrix)) {
        return 0;
    }
    if (matrix.rows == 1) {
        return '>';
    }
    if (i == 1) {
        return '\\';
    }
    if (i == matrix.rows) {
        return '/';
    }
    return '|';
}

void
wjb_m_print(const wjb_matrix matrix, const char *format)
{
    wjb_m_fprint(stdout, matrix, format);
}

void
wjb_m_fprint(FILE *file, const wjb_matrix matrix, const char *format)
{
    for (unsigned int i = 1; i <= matrix.rows; ++i) {
        fprintf(file, "%c ", left_bracket(matrix, i));
        for (unsigned int j = 1; j <= matrix.cols; ++i) {
            fprintf(file, format, wjb_m_get(matrix, i, j));
        }
        fprintf(file, "%c", right_bracket(matrix, i));
    }
}
