/*
 * Copyright Billy Brown 2016
 */

#include "minunit.h"
#include "wjb_m_operate.c"

static char *
m_add_test(void)
{
    wjb_matrix m1 = wjb_m_make(0, 0);
    mu_assert(
            "[wjb_matrix] ERROR m_add_test @ wjb_m_add(m1, 5)\n"
            "             expected not valid (0)",
            !wjb_m_valid(wjb_m_add(m1, 5)));

    wjb_m_free(&m1);

    wjb_matrix m2 = wjb_m_make(3, 2);

    wjb_m_set(m2, 1, 1, 3); wjb_m_set(m2, 1, 2, 2);
    wjb_m_set(m2, 2, 1, 1); wjb_m_set(m2, 2, 2, 4);
    wjb_m_set(m2, 3, 1, 0); wjb_m_set(m2, 3, 2, 1);

    wjb_matrix m = wjb_m_add(m2, 5);

    mu_assert(
            "[wjb_matrix] ERRPR m_add_test @ wjb_m_add(m2, 5)\n"
            "             expected 8, 7, 6, 9, 5, 6",
            wjb_m_get(m, 1, 1) == 8 && wjb_m_get(m, 1, 2) == 7 &&
            wjb_m_get(m, 2, 1) == 6 && wjb_m_get(m, 2, 2) == 9 &&
            wjb_m_get(m, 3, 1) == 5 && wjb_m_get(m, 3, 2) == 6);

    wjb_m_free(&m2);
    wjb_m_free(&m);

    return 0;
}

static char *
m_sum_test(void)
{

    wjb_matrix m1 = wjb_m_make(2, 2);
    wjb_matrix m2 = wjb_m_make(2, 2);

    wjb_m_set(m1, 1, 1, 3); wjb_m_set(m1, 1, 2, 2);
    wjb_m_set(m1, 2, 1, 1); wjb_m_set(m1, 2, 2, 4);

    wjb_m_set(m2, 1, 1, 1); wjb_m_set(m2, 1, 2, 1);
    wjb_m_set(m2, 2, 1, 0); wjb_m_set(m2, 2, 2, 2);

    wjb_matrix m = wjb_m_sum(m1, m2);

    mu_assert(
            "[wjb_matrix] ERROR m_sum_test @ m.rows, m.cols\n"
            "             expected 2, 2",
            m.rows == 2 && m.cols == 2);

    mu_assert(
            "[wjb_matrix] ERROR m_sum_test @ values\n"
            "             expected 4, 3, 1, 6",
            wjb_m_get(m, 1, 1) == 4 && wjb_m_get(m, 1, 2) == 3 &&
            wjb_m_get(m, 2, 1) == 1 && wjb_m_get(m, 2, 2) == 6);

    wjb_matrix too_small = wjb_m_make(2, 3);
    mu_assert(
            "[wjb_matrix] ERROR m_sum_test @ wjb_m_sum(m1, m)\n"
            "             expected invalid (0)",
            !wjb_m_valid(wjb_m_sum(m1, too_small)));

    wjb_m_free(&m1);
    wjb_m_free(&m2);
    wjb_m_free(&m);
    wjb_m_free(&too_small);

    return 0;
}

static char *
m_multiply_test(void)
{
    wjb_matrix m1 = wjb_m_make(0, 0);
    mu_assert(
            "[wjb_matrix] ERROR m_multiply_test @ wjb_m_multiply(m1, 5)\n"
            "             expected not valid (0)",
            !wjb_m_valid(wjb_m_add(m1, 5)));

    wjb_m_free(&m1);

    wjb_matrix m2 = wjb_m_make(3, 2);

    wjb_m_set(m2, 1, 1, 3); wjb_m_set(m2, 1, 2, 2);
    wjb_m_set(m2, 2, 1, 1); wjb_m_set(m2, 2, 2, 4);
    wjb_m_set(m2, 3, 1, 0); wjb_m_set(m2, 3, 2, 1);

    wjb_matrix m = wjb_m_multiply(m2, 2);

    mu_assert(
            "[wjb_matrix] ERRPR m_multiply_test @ wjb_m_multiply(m2, 2)\n"
            "             expected 6, 4, 2, 8, 0, 2",
            wjb_m_get(m, 1, 1) == 6 && wjb_m_get(m, 1, 2) == 4 &&
            wjb_m_get(m, 2, 1) == 2 && wjb_m_get(m, 2, 2) == 8 &&
            wjb_m_get(m, 3, 1) == 0 && wjb_m_get(m, 3, 2) == 2);

    wjb_m_free(&m2);
    wjb_m_free(&m);

    return 0;
}

static char *
m_product_test(void)
{

    wjb_matrix m1 = wjb_m_make(3, 2);
    wjb_matrix m2 = wjb_m_make(2, 1);

    wjb_m_set(m1, 1, 1, 3); wjb_m_set(m1, 1, 2, 2);
    wjb_m_set(m1, 2, 1, 1); wjb_m_set(m1, 2, 2, 4);
    wjb_m_set(m1, 3, 1, 0); wjb_m_set(m1, 3, 2, 1);

    wjb_m_set(m2, 1, 1, 2);
    wjb_m_set(m2, 2, 1, 3);

    wjb_matrix m = wjb_m_product(m1, m2);

    mu_assert(
            "[wjb_matrix] ERROR m_product_test @ m.rows, m.cols\n"
            "             expected 3, 1",
            m.rows == 3 && m.cols == 1);

    mu_assert(
            "[wjb_matrix] ERROR m_product_test @ values\n"
            "             expected 12, 14, 3",
            wjb_m_get(m, 1, 1) == 12 &&
            wjb_m_get(m, 2, 1) == 14 &&
            wjb_m_get(m, 3, 1) == 3);

    mu_assert(
            "[wjb_matrix] ERROR m_product_test @ wjb_m_product(m1, m)\n"
            "             expected invalid (0)",
            !wjb_m_valid(wjb_m_product(m1, m)));

    wjb_m_free(&m1);
    wjb_m_free(&m2);
    wjb_m_free(&m);

    return 0;
}

char *
m_operate_tests(void)
{
    mu_run_test(m_add_test);
    mu_run_test(m_sum_test);
    mu_run_test(m_multiply_test);
    mu_run_test(m_product_test);
    return 0;
}
